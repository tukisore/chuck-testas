import React, { Component } from 'react';
import axios from 'axios';

class Search extends Component {
  constructor(props){
    super(props)
    this.unmounted = false;
    this.state = {
      jokes: [],
      filter: "0-10",
      search: '',
    };
  }
  componentDidMount(){
    this.loadJoke();
  }

  componentWillUnmount(){
    this.unmounted = true;
  }
  loadJoke(){
    if(this.state.jokes.length<100){
      axios.get(`https://api.chucknorris.io/jokes/random`).then((e)=>{
        e.data.rating = Math.round(Math.random() * 10); 
        if(!this.unmounted){
          this.setState({jokes: [...this.state.jokes,e.data]});
          this.loadJoke();
        }
      });
    }
   
  }
  render() {
    const min = parseInt(this.state.filter.split("-")[0], 10);
    const max = parseInt(this.state.filter.split("-")[1], 10);
    return ( 
    <div className="">
        <p>
          <span className="voteBTN" onClick={this.props.nextStep}>Back!</span> 
        </p>
        <table className="fullWidth" >
          <thead>
            <tr>
              <th></th>
              <th><input className="fullWidth" value={this.state.search} onChange={e=>this.setState({search: e.target.value})}
              placeholder="search"/></th>
              <th>
                <select  value={this.state.filter} onChange={e => this.setState({filter: e.target.value})}>
                  <option value="0-10">0-10</option>
                  <option value="0-2">0-2</option>
                  <option value="2-6">2-6</option>
                  <option value="6-10">6-10</option>
                </select>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.jokes.filter(i => -1 < i.value.search(this.state.search)).filter(i => ((i.rating < max) && (i.rating > min))).map((i,index) => {
              return (
                <tr key={index}>
                  <td><img alt="Chuck" src={i.icon_url}/></td>
                  <td>{i.value}</td>
                  <td>{i.rating}</td>
                </tr>
                );
            })}
          </tbody>
        </table>
    </div>
  );
  }
}

export default  (Search);
