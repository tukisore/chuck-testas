import React, { Component } from 'react';
import './App.css';
import Joke from './Joke';
import WellCome from './WellCome';
import Bye from './Bye';
import Search from './Search';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      rand: 0,
      step: 0,
      joke: undefined,
    };
  }
  toSearchPage(){
    this.setState({step:13});
  }
  nextStep() {
    this.setState({
      rand: Math.random(),
      step: this.state.step > 10? 0: this.state.step + 1,
      joke: undefined,
    });
    axios.get(`https://api.chucknorris.io/jokes/random`)
      .then(res => {
        this.setState({joke: res.data});
    });
  };

  getRandomJone(joke){

  }

  render() {
    return (
      <div className="App">
        {(this.state.step === 0) && (
          <WellCome 
          nextStep={()=>this.nextStep()}
          toSearchPage={()=>this.toSearchPage()}
          />
        )} 
        {((this.state.step > 0) && (this.state.step < 11)) && (
          <Joke 
            rand={this.state.rand}
            funy={()=>this.nextStep()}
            notFuny={()=>this.nextStep()}
            joke={this.state.joke}
          />
        )}
         {(this.state.step === 11) && (
          <Bye nextStep={()=>this.nextStep()}/>
        )} 
        {(this.state.step === 13) && (
          <Search nextStep={()=>this.nextStep()}/>
        )}     
          
      </div>
    );
  }
}

export default App;
