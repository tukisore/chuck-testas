import React, { Component } from 'react';

class Joke extends Component {
  render() {
    return (<div className="">
      <div><img alt="Chuck" src={this.props.joke && this.props.joke.icon_url}/></div>
      <div>{this.props.joke && this.props.joke.value}</div>
      <div className="voteContainer">
        <span className="voteBTN" onClick={this.props.funy}>Funy</span> 
        <span className="voteBTN notFunny" onClick={this.props.notFuny}>Not Funy</span>
       </div>
    </div>);
  }
}

export default Joke;
